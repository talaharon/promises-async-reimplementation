export declare const all: (iterable: Iterable<unknown> | Promise<Iterable<unknown>>) => Promise<unknown>;
interface IGeneralObject {
    [key: string]: any;
}
export declare const props: (obj: IGeneralObject | Promise<IGeneralObject>) => Promise<unknown>;
export {};

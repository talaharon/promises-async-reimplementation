import { expect } from "chai";
import {race,some} from "../src/promise.race";
import {delay} from "../src/promise.delay";


function createPromise(operation:string, ms:number){
    return new Promise( (resolve,reject) => {
                        delay(ms)
                                .then(()=> {
                                    if(operation === "reject"){
                                        reject("Failed with value test");
                                    }
                                    resolve(operation);
                                });
                    });
}

describe("Promise Race/Some", function() {
    context("Race", function() {
        it("should exist", function() {
            expect(race).to.be.a("function");
        });
        it("should run as the quickest task", async function() {
            const start = new Date();
            await race([createPromise("", 2000), createPromise("",3000),createPromise("",1000)]);
            expect((Number(new Date()) - Number(start))).to.be.lessThan(1050);
        });
        it("should be equal to test3", async function() {
            expect(await race([createPromise("test1", 2000), createPromise("test2",3000),createPromise("test3",1000)])).to.deep.equal("test3");
        });
        it("should be equal to 2", async function() {
            expect(await race([2, createPromise("3",1000),createPromise("1",1000)])).to.be.equal(2);
        });
        it("should resolve the array and be equal to 1", async function() {
            expect(await race(delay(500,[createPromise("2",2000), createPromise("3",1000),createPromise("1",500)]))).to.be.equal("1");
        });
        it("should throw an error",  async function () {
            try{
                await race([ createPromise("reject",500),createPromise("world!",1000)]);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });

    context("Some", function() {
        it("should exist", function() {
            expect(some).to.be.a("function");
        });
        it("should run as quick as the second quickest task", async function() {
            const start = new Date();
            await some([createPromise("", 2000), createPromise("",3000),createPromise("",1000)],2);
            expect((Number(new Date()) - Number(start))).to.be.lessThan(2050);
        });
        it("should be equal to [test3,test1]", async function() {
            expect(await some([createPromise("test1", 2000), createPromise("test2",3000),createPromise("test3",1000)],2)).to.deep.equal(["test3","test1"]);
        });
        it("should be equal to [2, '3', '2']", async function() {
            expect(await some([2, createPromise("2",1000),createPromise("3",300)],3)).to.deep.equal([2,"3","2"]);
        });
        it("should resolve the array and be equal to [1]", async function() {
            expect(await some(delay(500,[createPromise("2",2000), createPromise("3",1000),createPromise("1",500)]),1)).to.deep.equal(["1"]);
        });
        it("should throw an error",  async function () {
            try{
                await some([createPromise("reject",500),createPromise("world!",1000)],1);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });

});

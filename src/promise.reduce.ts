
export const reduce = async (
    iterable: Iterable<unknown> | Promise<Iterable<unknown>>,
    cb: (acc: any, curr: any) => any,
    initial?: any
) => {
    iterable = await iterable;
    let i = 0;
    let aggregator = initial || (iterable as Array<unknown>)[i++];
    aggregator = await aggregator;
    for (i; i < Object.keys(iterable).length; i++) {
        aggregator = await cb(aggregator, (iterable as Array<unknown>)[i]);
    }

    return aggregator;
};

import { expect } from "chai";
import { all, props } from "../src/promise.all";
import { delay } from "../src/promise.delay";



function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Failed with value test");
                    // reject("Failed with value test");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("Promise All/Props", function () {
    context("All", function () {
        it("should exist", function () {
            expect(all).to.be.a("function");
        });
        it("should be parallel", async function () {
            const start = new Date();
            await all([createPromise("", 2000), createPromise("", 3000), createPromise("", 1000)]);
            expect((Number(new Date()) - Number(start))).to.be.lessThan(3050);
        });
        it("should be equal to \"test1\",\"test2\",\"test3\"", async function () {
            expect(await all([createPromise("test1", 2000), createPromise("test2", 3000), createPromise("test3", 1000)])).to.deep.equal(["test1", "test2", "test3"]);
        });
        it("should be equal to [2,\"3\",1]", async function () {
            expect(await all([2, createPromise("3", 1000), 1])).to.deep.equal([2, "3", 1]);
        });
        it("should resolve the array and be equal to [2,\"3\",1]", async function () {
            expect(await all(delay(2000, [2, createPromise("3", 1000), 1]))).to.deep.equal([2, "3", 1]);
        });
        it("should throw an error",  async function () {
            try{
                await all([2, createPromise("reject", 1000), 1]);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
            // expect( await all([
            //     createPromise("first", 10), 
            //     createPromise("reject", 1000), 
            //     createPromise("third", 10)
            // ])).to.throw("Failed with value test");
        });
    });

    context("Props", function () {
        it("should exist", function () {
            expect(props).to.be.a("function");
        });
        it("should be parallel", async function () {
            const start = new Date();
            await props({ "1": createPromise("test1", 2000), "2": createPromise("test2", 3000), "3": createPromise("test3", 1000) });
            expect((Number(new Date()) - Number(start))).to.be.lessThan(3050);
        });
        it("should be equal to {1:\"test1\",2:\"test2\",3:\"test3\"}", async function () {
            expect(await props({ "1": createPromise("test1", 2000), "2": createPromise("test2", 3000), "3": createPromise("test3", 1000) })).to.deep.equal({ "1": "test1", "2": "test2", "3": "test3" });
        });
        it("should be equal to {name:test,last:testeron,age:5}", async function () {
            expect(await props({ name: createPromise("test", 1000), last: createPromise("testeron", 500), age: 5 })).to.deep.equal({ name: "test", last: "testeron", age: 5 });
        });
        it("should work on arrays and be converted to {0:\"test\",1:\"testeron\",2:5}", async function () {
            expect(await props([createPromise("test", 1000), createPromise("testeron", 500), 5])).to.deep.equal({ 0: "test", 1: "testeron", 2: 5 });
        });
        it("should resolve the object and be equal to {name:test,last:testeron,age:5}", async function () {
            expect(await props(delay(2000, { name: createPromise("test", 1000), last: createPromise("testeron", 500), age: 5 }))).to.deep.equal({ name: "test", last: "testeron", age: 5 });
        });
        it("should throw an error",  async function () {
            try{
                await props([2, createPromise("reject", 1000), 1]);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });
});

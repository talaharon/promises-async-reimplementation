import log from "@ajar/marker";
import {delay} from "../src/promise.delay";

export const echo = async (msg: string, ms: number) => {
    log.yellow(`--> start ${msg}`);
    await delay(ms);
    log.blue(`finish <-- ${msg}`);
    return msg;
};

//--------------------------------------------------

export const random = (max: number, min = 0) =>
    min + Math.round(Math.random() * (max - min));

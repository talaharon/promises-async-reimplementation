import { expect } from "chai";
import { mapSeries, mapParallel } from "../src/promise.map";
import { delay } from "../src/promise.delay";



function createPromise(operation: any, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    reject("Failed with value test");
                }
                resolve(operation);
            });
    });
}

describe("Promise Map Parallel/Series", function () {
    context("Map Paralell", function () {
        it("should exist", function () {
            expect(mapParallel).to.be.a("function");
        });
        it("should be parallel", async function () {
            const start = new Date();
            await mapParallel([createPromise("", 0), createPromise("", 0), createPromise("", 0)], async () => true);
            expect((Number(new Date()) - Number(start))).to.be.lessThan(3050);
        });
        it("should return [2,3,4]", async function () {
            expect(await mapParallel([createPromise(1, 2000), createPromise(2, 3000), createPromise(3, 1000)], async (item: Promise<number>) => {
                return (await item) + 1;
            })).to.deep.equal([2, 3, 4]);
        });
        it("should return [\"Test!!!\", \"Yohoo!!!\",\"Google!!!\"]", async function () {
            expect(await mapParallel([createPromise("Test", 2000), "Yohoo", createPromise("Google", 1000)], async (item: string) => {
                return (await item)+"!!!";
            })).to.deep.equal(["Test!!!", "Yohoo!!!","Google!!!"]);
        });
        it("should throw an error",  async function () {
            try{
                await mapParallel(["Hell", createPromise("reject",3000),createPromise("world!",1000)],async (item)=>item);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });

    context("Map Series", function () {
        it("should exist", function () {
            expect(mapSeries).to.be.a("function");
        });
        it("should be series", async function () {
            const start = new Date();
            await mapSeries([createPromise("", 0), createPromise("", 0), createPromise("", 0)], async () => await delay(2000));
            expect((Number(new Date()) - Number(start))).to.be.greaterThan(6000);
        });
        it("should return [2,3,4]", async function () {
            expect(await mapSeries([createPromise(1, 2000), createPromise(2, 3000), createPromise(3, 1000)], async (item: Promise<number>) => {
                return (await item) + 1;
            })).to.deep.equal([2, 3, 4]);
        });
        it("should return [\"Test!!!\", \"Yohoo!!!\",\"Google!!!\"]", async function () {
            expect(await mapSeries([createPromise("Test", 2000), "Yohoo", createPromise("Google", 1000)], async (item: string) => {
                return (await item)+"!!!";
            })).to.deep.equal(["Test!!!", "Yohoo!!!","Google!!!"]);
        });
        it("should throw an error",  async function () {
            try{
                await mapSeries(["Hell", createPromise("reject",3000),createPromise("world!",1000)],async (item)=>item);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });


});


export const filterSeries = async (
    iterable: Iterable<unknown> | Promise<Iterable<unknown>>,
    filterer: (item: any, index: number, length: number) => Promise<boolean>
):Promise<any[]> => {
    iterable = await iterable;
    const results = [];
    const filterResults: boolean[] = [];
    for (const [index, item] of Object.entries(iterable)) {
        results[Number(index)] = await item;
        filterResults[Number(index)] = await filterer(
            results[Number(index)],
            Number(index),
            Object.keys(iterable).length
        );
    }
    return results.filter((item, index) => filterResults[index]);
};


export const filterParallel = async (
    iterable: Iterable<any> | Promise<Iterable<any>>,
    filterer: (item: any, index?: number, length?: number) => Promise<boolean>
):Promise<any[]> => {
    iterable = await iterable;
    const results = [];
    const filterResults = [];
    for (const [index, item] of Object.entries(iterable)) {
        filterResults[Number(index)] = filterer(
            item,
            Number(index),
            Object.keys(iterable).length
        );
    }
    for (const [index, item] of Object.entries(iterable)) {
        if (await filterResults[Number(index)]) {
            results.push(await item);
        }
    }
    return results;
};



export const each = async (
    iterable: Iterable<unknown> | Promise<Iterable<unknown>>,
    iterator: (val: unknown, index: number, arrayLength: number) => unknown
): Promise<unknown[]> => {
    iterable = await iterable;
    const resultsArr: unknown[] = [];
    for (const [index, item] of Object.entries(iterable)) {
        resultsArr[Number(index)] = item;
        await iterator(item, Number(index), Object.keys(iterable).length);
    }
    return resultsArr;
};

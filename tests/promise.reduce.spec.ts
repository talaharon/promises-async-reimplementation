import { expect } from "chai";
import { reduce } from "../src/promise.reduce";
import { delay } from "../src/promise.delay";



function createPromise(operation: any, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    reject("Failed with value test");
                }
                resolve(operation);
            });
    });
}

describe("Promise Reduce", function () {
    context("Reduce", function () {
        it("should exist", function () {
            expect(reduce).to.be.a("function");
        });
        it("should be series", async function () {
            const start = new Date();
            await reduce([createPromise("", 0), createPromise("", 0), createPromise("", 0)], async () => delay(1000));
            expect((Number(new Date()) - Number(start))).to.be.greaterThan(2000);
        });
        it("should return 6", async function () {
            expect(await reduce([createPromise(1, 2000), createPromise(2, 3000), createPromise(3, 1000)], async (acc,curr) => {
                return acc + (await curr);
            })).to.be.equal(6);
        });
        it("should return 8", async function () {
            expect(await reduce([createPromise(1, 2000), createPromise(2, 3000), createPromise(3, 1000)], async (acc,curr) => {
                return acc + (await curr);
            },2)).to.be.equal(8);
        });
        it("should return 'abc'", async function () {
            expect(await reduce([createPromise("a", 2000), "b", createPromise("c", 1000)], async (acc,curr) => {
                return acc + await curr;
            })).to.deep.equal("abc");
        });
        it("should return 'zabc'", async function () {
            expect(await reduce([createPromise("a", 2000), "b", createPromise("c", 1000)], async (acc,curr) => {
                return acc + await curr;
            },"z")).to.deep.equal("zabc");
        });
        it("should throw an error",  async function () {
            try{
                await reduce([createPromise("reject",500),createPromise("world!",1000)],item=>item);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });

    


});

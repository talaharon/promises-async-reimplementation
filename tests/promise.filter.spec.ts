import { expect } from "chai";
import { filterSeries, filterParallel } from "../src/promise.filter";
import { delay } from "../src/promise.delay";



function createPromise(operation: any, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    reject("Failed with value test");
                }
                resolve(operation);
            });
    });
}

describe("Promise Filter Parallel/Series", function () {
    context("Filter Paralell", function () {
        it("should exist", function () {
            expect(filterParallel).to.be.a("function");
        });
        it("should be parallel", async function () {
            const start = new Date();
            await filterParallel([createPromise("", 2000), createPromise("", 3000), createPromise("", 1000)], async () => true);
            expect((Number(new Date()) - Number(start))).to.be.lessThan(3050);
        });
        it("should return [2,3]", async function () {
            expect(await filterParallel([createPromise(1, 2000), createPromise(2, 3000), createPromise(3, 1000)], async (item: number) => {
                return (await item) >= 2;
            })).to.deep.equal([2, 3]);
        });
        it("should return ['Test', 'Test it until you fake it']", async function () {
            expect(await filterParallel([createPromise("Test", 2000), "Yohoo", createPromise("Google", 1000), createPromise("Test it until you make it", 1000)], async (item: string) => {
                return (await item).toLowerCase().includes("test");
            })).to.deep.equal(["Test", "Test it until you make it"]);
        });
        it("should throw an error",  async function () {
            try{
                await filterParallel(["Hell", createPromise("reject",3000),createPromise("world!",1000)],async (item)=>item);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });

    context("Filter Series", function () {
        it("should exist", function () {
            expect(filterSeries).to.be.a("function");
        });
        it("should be series", async function () {
            const start = new Date();
            await filterSeries([createPromise("", 0), createPromise("", 0), createPromise("", 0)], async () => {
                await delay(2000);
                return true;
            });
            expect((Number(new Date()) - Number(start))).to.be.greaterThan(6000);
        });
        it("should return [2,3]", async function () {
            expect(await filterSeries([createPromise(1, 2000), createPromise(2, 3000), createPromise(3, 1000)], async (item: Promise<number> | number) => {
                return (await item) >= 2;
            })).to.deep.equal([2, 3]);
        });
        it("should return ['Test', 'Test it until you fake it']", async function () {
            expect(await filterSeries([createPromise("Test", 2000), "Yohoo", createPromise("Google", 1000), createPromise("Test it until you make it", 1000)], async (item: Promise<string> | string) => {
                return (await item).toLowerCase().includes("test");
            })).to.deep.equal(["Test", "Test it until you make it"]);
        });
        it("should throw an error",  async function () {
            try{
                await filterSeries(["Hell", createPromise("reject",3000),createPromise("world!",1000)],async (item)=>item);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });


});

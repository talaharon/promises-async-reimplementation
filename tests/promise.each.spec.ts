import { expect } from "chai";
import {each} from "../src/promise.each";
import {delay} from "../src/promise.delay";



function createPromise(operation:any, ms:number){
    return new Promise( (resolve,reject) => {
                        delay(ms)
                                .then(()=> {
                                    if(operation === "reject"){
                                        reject("Failed with value test");
                                    }
                                    resolve(operation);
                                });
                    });
}

describe("Promise Each", function() {
    context("Each", function() {
        it("should exist", function() {
            expect(each).to.be.a("function");
        });
        it("should be series", async function() {
            const start = new Date();
            await each([createPromise("", 0), createPromise("",0),createPromise("",0)],async ()=>await delay(1000));
            expect((Number(new Date()) - Number(start))).to.be.greaterThanOrEqual(3000);
        });
        it("should be equal to 6", async function() {
            let count = 0;
            await each([createPromise(1, 2000), createPromise(2,3000),createPromise(3,1000)],async (item)=>count+=Number(await item) );
            expect(count).to.be.equal(6);
        });
        it("should be equal to 'Hello world!", async function() {
            let res = "";
            await each(["Hell", createPromise("o ",3000),createPromise("world!",1000)],async (item)=>res+=await item);
            expect(res).to.be.equal("Hello world!");
        });
        it("should throw an error",  async function () {
            try{
                await each(["Hell", createPromise("reject",3000),createPromise("world!",1000)],async (item)=>item);
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });


});

/***************************
// Async/Await Implementation
***************************/
export declare const reduce: (iterable: Iterable<unknown> | Promise<Iterable<unknown>>, cb: (acc: any, curr: any) => any, initial?: any) => Promise<any>;

/***************************
// Async/Await Implementation
***************************/
export declare const mapParallel: (iterable: Iterable<any> | Promise<Iterable<any>>, mapper: (item: any, index: number, length: number) => any) => Promise<any[]>;
/***************************
// Async/Await Implementation
***************************/
export declare const mapSeries: (iterable: Iterable<any> | Promise<Iterable<any>>, mapper: (item: any, index: number, length: number) => any) => Promise<any[]>;

export declare const race: (iterable: Iterable<unknown> | Promise<Iterable<unknown>>) => Promise<unknown>;
export declare const some: (iterable: Iterable<unknown> | Promise<Iterable<unknown>>, count: number) => Promise<unknown[]>;

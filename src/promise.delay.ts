export const delay = async (
    ms: number,
    val: any | Promise<unknown> = undefined
) => {
    const res = await val;
    await new Promise((resolve) => setTimeout(resolve, ms));
    return res;
};


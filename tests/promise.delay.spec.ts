import {delay} from "../src/promise.delay";
import { expect } from "chai";

describe("Promise Delay", function() {
    context("Delay", function() {
        it("should exist", function() {
            expect(delay).to.be.a("function");
        });
        it("should wait 6 seconds", async function() {
            const start = new Date();
            await delay(6000);
            expect((Number(new Date()) - Number(start))).to.be.greaterThanOrEqual(6000);
        });
        it("should resolve with the value 6", async function() {
            expect(await delay(1000,6)).to.be.equal(6);
        });
        it("should resolve with the value 'test'", async function() {
            expect(await delay(1000,"test")).to.be.equal("test");
        });
        it("should throw an error",  async function () {
            try{
                await delay(2000,new Promise((resolved,reject) => 
                reject("Failed with value test")));
            }catch(err){
                expect(err).to.be.equal("Failed with value test");
            }
        });
    });
});

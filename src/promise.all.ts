
export const all = async (
    iterable: Iterable<unknown> | Promise<Iterable<unknown>>
): Promise<unknown> => {
    const results = [];
    iterable = await iterable;
    for (const [index, item] of Object.entries(iterable)) {
        results[Number(index)] = await item;

    }
    return results;
};

interface IGeneralObject {
    [key: string]: any;
}

export const props = async (
    obj: IGeneralObject | Promise<IGeneralObject>
): Promise<unknown> => {
    obj = await obj;
    const results = Object.assign({}, obj);

    for (const key of Object.keys(obj)) {
        const res = await obj[key];
        results[key] = res;
    }
    return results;
};
/***************************
// Async/Await Implementation
***************************/
export declare const each: (iterable: Iterable<unknown> | Promise<Iterable<unknown>>, iterator: (val: unknown, index: number, arrayLength: number) => unknown) => Promise<unknown[]>;

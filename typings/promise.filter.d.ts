/***************************
// Async/Await Implementation
***************************/
export declare const filterSeries: (iterable: Iterable<unknown> | Promise<Iterable<unknown>>, filterer: (item: any, index: number, length: number) => Promise<boolean>) => Promise<any[]>;
/***************************
// Async/Await Implementation
***************************/
export declare const filterParallel: (iterable: Iterable<any> | Promise<Iterable<any>>, filterer: (item: any, index?: number | undefined, length?: number | undefined) => Promise<boolean>) => Promise<any[]>;



export const race = (
    iterable: Iterable<unknown> | Promise<Iterable<unknown>>
) => {
    return new Promise((resolve) => {
        Promise.resolve(iterable).then((resolvedIterable) => {
            for (const [, item] of Object.entries(resolvedIterable)) {
                Promise.resolve(item).then((result) => {
                    resolve(result);
                });
            }
        });
    });
};

export const some = (
    iterable: Iterable<unknown> | Promise<Iterable<unknown>>,
    count: number
): Promise<unknown[]> => {
    return new Promise((resolve) => {
        const results: unknown[] = [];
        Promise.resolve(iterable).then((resolvedIterable) => {
            for (const [, item] of Object.entries(resolvedIterable)) {
                Promise.resolve(item).then((result) => {
                    results.push(result);
                    if (results.length === count) {
                        resolve(results);
                    }
                });
            }
        });
    });
};



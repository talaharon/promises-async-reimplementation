
export const mapParallel = async (
    iterable: Iterable<any> | Promise<Iterable<any>>,
    mapper: (item: any, index: number, length: number) => any
) => {
    iterable = await iterable;
    const results = [];
    for (const [index, item] of Object.entries(iterable)) {
        results[Number(index)] = mapper(
            item,
            Number(index),
            Object.keys(iterable).length
        );
    }

    for (let i = 0; i < results.length; i++) {
        results[i] = await results[i];
    }
    return results;
};

/***************************
// Async/Await Implementation
***************************/
export const mapSeries = async (
    iterable: Iterable<any> | Promise<Iterable<any>>,
    mapper: (item: any, index: number, length: number) => any
) => {
    await iterable;
    const resultsArr = [];
    for (const [index, item] of Object.entries(iterable)) {
        const res = await item;
        resultsArr[Number(index)] = await mapper(
            res,
            Number(index),
            Object.keys(iterable).length
        );
    }
    return resultsArr;
};

